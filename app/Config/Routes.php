<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');
$routes->get('/administrator/register', 'User::create', ['filter' => 'role:admin']);

$routes->get('/administrator', 'Administrator::profil', ['filter' => 'role:admin']);
$routes->get('/member', 'Member::index', ['filter' => 'role:user']);
$routes->get('/administrator/dashboard', 'Dashboard::index', ['filter' => 'role:admin']);
$routes->get('/administrator/profil', 'Administrator::index', ['filter' => 'role:admin']);

// USER
$routes->get('/administrator/user', 'User::index', ['filter' => 'role:admin']);
$routes->get('/administrator/user/create', 'User::create', ['filter' => 'role:admin']);
$routes->get('/administrator/user/edit/(:segment)', 'User::edit/$1', ['filter' => 'role:admin']);
$routes->delete('/administrator/user/delete/(:num)', 'User::delete/$1', ['filter' => 'role:admin']);
$routes->get('/berita/detail_berita/(:any)', 'Berita::detail/$1');
// BERITA
$routes->get('/administrator/news', 'News::index', ['filter' => 'role:admin']);
$routes->get('/administrator/news/create', 'News::create', ['filter' => 'role:admin']);
$routes->get('/administrator/news/edit/(:segment)', 'News::edit/$1', ['filter' => 'role:admin']);
$routes->delete('/administrator/news/delete/(:num)', 'News::delete/$1', ['filter' => 'role:admin']);
$routes->get('/berita/detail_berita/(:any)', 'Berita::detail/$1');
// GALERI
$routes->get('/administrator/galeries', 'Galeries::index', ['filter' => 'role:admin']);
$routes->get('/administrator/galeries/create', 'Galeries::create', ['filter' => 'role:admin']);
$routes->get('/administrator/galeries/edit/(:segment)', 'Galeries::edit/$1', ['filter' => 'role:admin']);
$routes->delete('/administrator/galeries/delete/(:num)', 'Galeries::delete/$1', ['filter' => 'role:admin']);
$routes->get('/album/(:any)', 'Album::detail/$1');
// SLIDER
$routes->get('/administrator/sliders', 'Sliders::index', ['filter' => 'role:admin']);
$routes->get('/administrator/sliders/create', 'Sliders::create', ['filter' => 'role:admin']);
$routes->get('/administrator/sliders/edit/(:segment)', 'Sliders::edit/$1', ['filter' => 'role:admin']);
$routes->delete('/administrator/sliders/delete/(:num)', 'Sliders::delete/$1', ['filter' => 'role:admin']);
$routes->get('/album/(:any)', 'Album::detail/$1');


// GROUPS
$routes->get('/administrator/group', 'Group::index', ['filter' => 'role:admin']);
$routes->get('/administrator/group/create', 'Group::create', ['filter' => 'role:admin']);
$routes->get('/administrator/group/edit/(:segment)', 'Group::edit/$1', ['filter' => 'role:admin']);
$routes->delete('/administrator/group/(:num)', 'Group::delete/$1');

$routes->get('/administrator', 'Administrator::index', ['filter' => 'role:admin']);
$routes->get('/administrator/member', 'User::index', ['filter' => 'role:admin']);
$routes->get('/administrator/member/create', 'User::create', ['filter' => 'role:admin']);
$routes->get('/administrator/member/(:num)', 'User::detail/$1', ['filter' => 'role:admin']);
$routes->get('/administrator/(:any)', 'Administrator::index', ['filter' => 'role:admin']);
// $routes->get('/member', 'User::index');
$routes->get('/administrator/setting/(:any)', 'Setting::profil/$1');
$routes->get('/administrator/logout', 'Administrator::logout');


// SLIDER
// $routes->get('/administrator/sliders', 'Sliders::index');
// $routes->get('/administrator/sliders/create', 'Sliders::create');
// $routes->get('/administrator/sliders/edit/(:segment)', 'Sliders::edit/$1');
// $routes->delete('/administrator/sliders/(:num)', 'Sliders::delete/$1');
// $routes->get('/administrator/sliders/(:any)', 'Sliders::detail/$1');

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
