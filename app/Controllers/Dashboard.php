<?php

namespace App\Controllers;

class Dashboard extends BaseController
{

  public function index()
  {
    $data = [
      'title' => 'News | Ayosinau',
      'lihat' => $this->settingModel->getSetting(),
      // 'user' => $this->userModel->getUser(),
    ];

    return view('admin/dashboard', $data);
  }
}
