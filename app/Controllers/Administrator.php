<?php

namespace App\Controllers;

class Administrator extends BaseController
{

    public function index()
    {
        $data = [
            'title' => 'News | Ayosinau',
            'lihat' => $this->settingModel->getSetting(),
            'setting' => $this->settingModel->getSetting()
        ];
        return view('admin/index', $data);
    }

    public function profil()
    {
        $data = [
            'title' => 'News | Ayosinau',
            'lihat' => $this->settingModel->getSetting(),
            'setting' => $this->settingModel->getSetting()
        ];
        return view('admin/profil', $data);
    }

    public function logout()
    {
        if ($this->auth->check()) {
            $this->auth->logout();
        }

        return redirect()->to('/');
    }
}
