<?php

namespace App\Controllers;

use App\Models\SlidersModel;

class Sliders extends BaseController
{
  protected $slidersModel;

  public function __construct()
  {
    $this->slidersModel = new SlidersModel();
  }

  public function index()
  {
    // $sliders = $this->slidersModel->findAll();
    $data = [
      'title' => 'List Slider',
      'sliders' => $this->slidersModel->getSliders(),
      'setting' => $this->settingModel->getSetting(),
      'lihat' => $this->settingModel->getSetting(),
    ];

    return view('sliders/index', $data);
  }

  public function create()
  {
    // session(); ditaruh di basecontroller
    $data = [
      'title' => 'Form Tambah Data Sliders | Ayosinau',
      'setting' => $this->settingModel->getSetting(),
      'lihat' => $this->settingModel->getSetting(),
      'validation' => \Config\Services::validation()
    ];

    return view('sliders/create', $data);
  }

  public function save()
  {
    // Validasi input
    if (!$this->validate([
      'title' => [
        'rules' => 'required|is_unique[sliders.title]',
        'errors' => [
          'required' => '{field} sliders harus diisi.',
          'is_unique' => '{field} sliders sudah terdaftar'
        ]
      ],
      'slider' => [
        'rules' => 'max_size[slider,1024]|is_image[slider]mime_in[slider,image/png,image/jpg,image/jpeg]]',
        'errors' => [
          'max_size' => 'Ukuran gambar terlalu besar',
          'is_image' => 'Yang anda pilih bukan gambar',
          'mime_in' => 'Yang anda pilih bukan gambar'
        ]
      ]
    ])) {

      return redirect()->to('/administrator/sliders/create')->withInput();
    }

    // Ambil gambar
    $fileSlider = $this->request->getFile('slider');
    // Apakah tidak ada gambar yang diupload
    if ($fileSlider->getError() == 4) {
      $namaSlider = 'default.jpg';
    } else {
      // generate nama slider random
      $namaSlider = $fileSlider->getRandomName();
      // Pindahlan file ke folder slider
      $fileSlider->move('slider', $namaSlider);
    }

    $slug = url_title($this->request->getVar('title'), '-', true);
    $this->slidersModel->save([
      'title' => $this->request->getVar('title'),
      'slug' => $slug,
      'body' => $this->request->getVar('body'),
      'judul_link' => $this->request->getVar('judul_link'),
      'link_judul' => $this->request->getVar('link_judul'),
      'judul_link_dua' => $this->request->getVar('judul_link_dua'),
      'link_judul_dua' => $this->request->getVar('link_judul_dua'),
      'status' => $this->request->getVar('status'),
      'created_at' => time(),
      'updated_at' => time(),
      'slider' => $namaSlider
    ]);

    session()->setFlashdata('pesan', 'Data berhasil ditambahkan.');

    return redirect()->to('/administrator/sliders');
  }

  public function delete($id)
  {
    // Cari gambar bedasarkan id
    $sliders = $this->slidersModel->find($id);

    // cek jika gambarnya default.jpg
    if ($sliders['slider'] != 'default.jpg') {
      // Hapus gambar
      unlink('slider/' . $sliders['slider']);
    }

    $this->slidersModel->delete($id);
    // session()->setFlashdata('pesan', 'Data berhasil dihapus.');

    return redirect()->to('/administrator/sliders');
  }

  public function edit($slug)
  {
    $data = [
      'title' => 'Form Ubah Data Slider',
      'validation' => \Config\Services::validation(),
      'sliders' => $this->slidersModel->getSliders($slug),
      'setting' => $this->settingModel->getSetting(),
      'lihat' => $this->settingModel->getSetting(),
    ];

    return view('sliders/edit', $data);
  }

  public function update($id)
  {
    //  Cek Judul
    $slidersLama = $this->slidersModel->getSliders($this->request->getVar('slug'));
    if ($slidersLama['title'] == $this->request->getVar('title')) {
      $rule_title = 'required';
    } else {
      $rule_title = 'required|is_unique[sliders.title]';
    }
    if (!$this->validate([
      'title' => [
        'rules' => $rule_title,
        'errors' => [
          'required' => '{field} sliders harus diisi.',
          'is_unique' => '{field} sliders sudah terdaftar'
        ]
      ],
      'slider' => [
        'rules' => 'max_size[slider,1024]|is_image[slider]mime_in[slider,image/png,image/jpg,image/jpeg]]',
        'errors' => [
          'max_size' => 'Ukuran gambar terlalu besar',
          'is_image' => 'Yang anda pilih bukan gambar',
          'mime_in' => 'Yang anda pilih bukan gambar'
        ]
      ]
    ])) {
      return redirect()->to('/sliders/edit/' . $this->request->getVar('slug'))->withInput();
    }

    $fileSlider = $this->request->getFile('slider');

    // Cek gambar, apakah tetap gambar lama
    if ($fileSlider->getError() == 4) {
      $namaSlider = $this->request->getVar('sliderLama');
    } else {
      // generate nama file random
      $namaSlider = $fileSlider->getRandomName();
      // pindahkan gambar
      $fileSlider->move('slider', $namaSlider);
      // Hapus file yang lama
      unlink('slider/' . $this->request->getVar('sliderLama'));
    }

    $slug = url_title($this->request->getVar('title'), '-', true);
    $this->slidersModel->save([
      'id' => $id,
      'title' => $this->request->getVar('title'),
      'slug' => $slug,
      'body' => $this->request->getVar('body'),
      'judul_link' => $this->request->getVar('judul_link'),
      'link_judul' => $this->request->getVar('link_judul'),
      'judul_link_dua' => $this->request->getVar('judul_link_dua'),
      'link_judul_dua' => $this->request->getVar('link_judul_dua'),
      'status' => $this->request->getVar('status'),
      'updated_at' => time(),
      'slider' => $namaSlider
    ]);

    session()->setFlashdata('pesan', 'Data berhasil diubah.');

    return redirect()->to('/administrator/sliders');
  }

  //--------------------------------------------------------------------

}
