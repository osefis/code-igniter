<?php

namespace App\Controllers;

use App\Models\UserModel;

class User extends BaseController
{
    protected $usersModel;

    public function __construct()
    {
        $this->userModel = new UserModel();
    }

    public function index()
    {
        $data = [
            'title' => 'Data Member',
            'lihat' => $this->settingModel->getSetting(),
            'user' => $this->userModel->getUser(),
            'setting' => $this->settingModel->getSetting()
        ];

        return view('user/index', $data);
    }

    public function detail($id = 0)
    {
        $data = [
            'title' => 'Detail Member',
            'lihat' => $this->settingModel->getSetting(),
            'setting' => $this->settingModel->getSetting()
        ];

        $this->builder->select('users.id as userid, nama, email, name, user_image, username');
        $this->builder->join('auth_groups_users', 'auth_groups_users.user_id = users.id');
        $this->builder->join('auth_groups', 'auth_groups.id = auth_groups_users.group_id');
        $this->builder->where('users.id', $id);

        $query = $this->builder->get();

        $data['user'] = $query->getRow();

        if (empty($data['user'])) {
            return redirect()->to('administrator');
        }

        return view('admin/detail_user', $data);
    }

    public function create()
    {
        $data = [
            'title' => 'Form Tambah Data Group',
            'setting' => $this->settingModel->getSetting(),
            'lihat' => $this->settingModel->getSetting(),
            'user' => $this->userModel->getUser(),
            'validation' => \Config\Services::validation()
        ];

        return view('user/create', $data);
    }

    public function save()
    {
        // Validasi input
        if (!$this->validate([
            'nama' => [
                'rules' => 'required|trim',
                'errors' => [
                    'required' => '{field} harus diisi.',
                    'trim' => '{field} trim'
                ]
            ],
            'email' => [
                'rules' => 'required|trim|valid_email|is_unique[users.email]',
                'errors' => [
                    'required' => '{field} harus diisi.',
                    'trim' => '{field} trim',
                    'valid_email' => '{field} harus valid',
                    'is_unique' => '{field} sudah terdaftar',
                ]
            ],
            'password_hash' => [
                'rules' => 'required|trim|min_length[6]',
                'errors' => [
                    'required' => '{field} harus diisi.',
                    'trim' => '{field} trim',
                    'min_length' => '{field} minimal 6 karakter'
                ]
            ],
            'user_image' => [
                'rules' => 'max_size[user_image,1024]|is_image[user_image]mime_in[user_image,image/png,image/jpg,image/jpeg]]',
                'errors' => [
                    'max_size' => 'Ukuran gambar terlalu besar',
                    'is_image' => 'Yang anda pilih bukan gambar',
                    'mime_in' => 'Yang anda pilih bukan gambar'
                ]
            ]
        ])) {

            return redirect()->to('/administrator/user/create')->withInput();
        }

        // Ambil gambar
        $fileUser_image = $this->request->getFile('user_image');
        // Apakah tidak ada gambar yang diupload
        if ($fileUser_image->getError() == 4) {
            $namaUser_image = 'avatar.png';
        } else {
            // generate nama image random
            $namaUser_image = $fileUser_image->getRandomName();
            // Pindahlan file ke folder user
            $fileUser_image->move('user', $namaUser_image);
        }

        $slug = url_title($this->request->getVar('nama'), '-', true);
        $this->userModel->save([
            'nama' => $this->request->getVar('nama'),
            'slug' => $slug,
            'username' => $this->request->getVar('username'),
            'email' => $this->request->getVar('email'),
            'password_hash' => password_hash($this->request->getPost('password_hash'), PASSWORD_DEFAULT),
            'role_id' => 2,
            'is_active' => 1,
            'active' => 1,
            'created_at' => time(),
            'updated_at' => time(),
            'user_image' => $namaUser_image
        ]);

        session()->setFlashdata('pesan', 'Data berhasil ditambahkan.');

        return redirect()->to('/administrator/user');
    }

    public function delete($id)
    {
        // Cari gambar bedasarkan id
        $user = $this->userModel->find($id);

        // cek jika gambarnya avatar.png
        if ($user['user_image'] != 'avatar.png') {
            // Hapus gambar
            unlink('user/' . $user['user_image']);
        }

        $this->userModel->delete($id);
        // session()->setFlashdata('pesan', 'Data berhasil dihapus.');

        return redirect()->to('/administrator/user');
    }
}
