<?php

namespace App\Controllers;

use App\Models\GroupModel;

class Group extends BaseController
{
    protected $groupModel;

    public function __construct()
    {
        $this->groupModel = new GroupModel();
    }

    public function index()
    {
        $data = [
            'title' => 'Group | Ayosinau',
            'group' => $this->groupModel->getGroup(),
            'lihat' => $this->settingModel->getSetting(),
            'setting' => $this->settingModel->getSetting(),
        ];

        return view('group/index', $data);
    }

    public function create()
    {
        $data = [
            'title' => 'Form Tambah Data Group | Ayosinau',
            'setting' => $this->settingModel->getSetting(),
            'lihat' => $this->settingModel->getSetting(),
            'validation' => \Config\Services::validation()
        ];

        return view('group/create', $data);
    }

    public function save()
    {
        // Validasi input
        if (!$this->validate([
            'name' => [
                'rules' => 'required|is_unique[auth_groups.name]',
                'errors' => [
                    'required' => '{field} group harus diisi.',
                    'is_unique' => '{field} group sudah terdaftar'
                ]
            ]
        ])) {

            return redirect()->to('/administrator/group/create')->withInput();
        }

        $slug = url_title($this->request->getVar('name'), '-', true);
        $this->groupModel->save([
            'name' => $this->request->getVar('name'),
            'slug' => $slug,
            'description' => $this->request->getVar('description'),
            'created_at' => time(),
            'updated_at' => time(),
        ]);

        session()->setFlashdata('pesan', 'Data berhasil ditambahkan.');

        return redirect()->to('/administrator/group');
    }

    public function delete($id)
    {

        $this->groupModel->delete($id);
        // session()->setFlashdata('pesan', 'Data berhasil dihapus.');

        return redirect()->to('/administrator/group');
    }

    public function edit($slug)
    {
        $data = [
            'title' => 'Form Ubah Data Group | Ayosinau',
            'validation' => \Config\Services::validation(),
            'setting' => $this->settingModel->getSetting(),
            'lihat' => $this->settingModel->getSetting(),
            'group' => $this->groupModel->getGroup($slug)
        ];

        return view('group/edit', $data);
    }

    public function update($id)
    {
        //  Cek Judul
        $groupLama = $this->groupModel->getGroup($this->request->getVar('slug'));
        if ($groupLama['name'] == $this->request->getVar('name')) {
            $rule_name = 'required';
        } else {
            $rule_name = 'required|is_unique[auth_groups.name]';
        }
        if (!$this->validate([
            'name' => [
                'rules' => $rule_name,
                'errors' => [
                    'required' => '{field} group harus diisi.',
                    'is_unique' => '{field} group sudah terdaftar'
                ]
            ]
        ])) {
            return redirect()->to('/group/edit/' . $this->request->getVar('slug'))->withInput();
        }

        $slug = url_title($this->request->getVar('name'), '-', true);
        $this->groupModel->save([
            'id' => $id,
            'name' => $this->request->getVar('name'),
            'slug' => $slug,
            'description' => $this->request->getVar('description'),
            'updated_at' => time(),
        ]);

        session()->setFlashdata('pesan', 'Data berhasil diubah.');

        return redirect()->to('/administrator/group');
    }

    //--------------------------------------------------------------------

}
