<?php

namespace App\Controllers;

class Setting extends BaseController
{

  public function profil($slug)
  {

    $data = [
      'title' => 'Profil',
      'validation' => \Config\Services::validation(),
      'setting' => $this->settingModel->getSetting($slug),
      'lihat' => $this->settingModel->getSetting(),
      'user' => $this->usersModel->getUser(),
    ];

    return view('admin/setting', $data);
  }

  public function update($id)
  {
    //  Cek Judul
    $settingLama = $this->settingModel->getSetting($this->request->getVar('slug'));
    if ($settingLama['sekolah'] == $this->request->getVar('sekolah')) {
      $rule_sekolah = 'required';
    } else {
      $rule_sekolah = 'required|is_unique[setting.sekolah]';
    }
    if (!$this->validate([
      'sekolah' => [
        'rules' => $rule_sekolah,
        'errors' => [
          'required' => '{field} setting harus diisi.',
          'is_unique' => '{field} setting sudah terdaftar'
        ]
      ],
      'fotokepsek' => [
        'rules' => 'max_size[fotokepsek,1024]|is_image[fotokepsek]|mime_in[fotokepsek,image/jpg,image/jpeg,image/png]',
        'errors' => [
          'max_size' => 'Ukuran gambar terlalu besar',
          'is_image' => 'Yang anda pilih bukan gambar',
          'mime_in' => 'Yang anda pilih bukan gambar'
        ]
      ],
      'logo' => [
        'rules' => 'max_size[logo,1024]|is_image[logo]|mime_in[logo,image/jpg,image/jpeg,image/png]',
        'errors' => [
          'max_size' => 'Ukuran gambar terlalu besar',
          'is_image' => 'Yang anda pilih bukan gambar',
          'mime_in' => 'Yang anda pilih bukan gambar'
        ]
      ],
      'banner' => [
        'rules' => 'max_size[banner,1024]|is_image[banner]|mime_in[banner,image/jpg,image/jpeg,image/png]',
        'errors' => [
          'max_size' => 'Ukuran gambar terlalu besar',
          'is_image' => 'Yang anda pilih bukan gambar',
          'mime_in' => 'Yang anda pilih bukan gambar'
        ]
      ]
    ])) {
      return redirect()->to('/administrator/setting/' . $this->request->getVar('slug'))->withInput();
    }

    $fileLogo = $this->request->getFile('logo');

    // Cek gambar, apakah tetap gambar lama
    if ($fileLogo->getError() == 4) {
      $namaLogo = $this->request->getVar('logoLama');
    } else {
      // generate nama file random
      $namaLogo = $fileLogo->getRandomName();
      // pindahkan gambar
      $fileLogo->move('setting', $namaLogo);
      // Hapus file yang lama
      unlink('setting/' . $this->request->getVar('logoLama'));
    }

    $fileFotokepsek = $this->request->getFile('fotokepsek');

    // Cek gambar, apakah tetap gambar lama
    if ($fileFotokepsek->getError() == 4) {
      $namaFotokepsek = $this->request->getVar('fotokepsekLama');
    } else {
      // generate nama file random
      $namaFotokepsek = $fileFotokepsek->getRandomName();
      // pindahkan gambar
      $fileFotokepsek->move('setting', $namaFotokepsek);
      // Hapus file yang lama
      unlink('setting/' . $this->request->getVar('fotokepsekLama'));
    }

    $fileBanner = $this->request->getFile('banner');

    // Cek gambar, apakah tetap gambar lama
    if ($fileBanner->getError() == 4) {
      $namaBanner = $this->request->getVar('bannerLama');
    } else {
      // generate nama file random
      $namaBanner = $fileBanner->getRandomName();
      // pindahkan gambar
      $fileBanner->move('setting', $namaBanner);
      // Hapus file yang lama
      unlink('setting/' . $this->request->getVar('bannerLama'));
    }

    $slug = url_title($this->request->getVar('sekolah'), '-', true);
    $this->settingModel->save([
      'id' => $id,
      'sekolah' => $this->request->getPost('sekolah'),
      'slug' => $slug,
      'kepsek' => $this->request->getVar('kepsek'),
      'pengantar' => $this->request->getVar('pengantar'),
      'visimisi' => $this->request->getVar('visimisi'),
      'sejarah' => $this->request->getPost('sejarah'),
      'lokasi' => $this->request->getVar('lokasi'),
      'video' => $this->request->getVar('video'),
      'telp' => $this->request->getVar('telp'),
      'alamat' => $this->request->getVar('alamat'),
      'facebook' => $this->request->getVar('facebook'),
      'twitter' => $this->request->getVar('twitter'),
      'instagram' => $this->request->getVar('instagram'),
      'fotokepsek' => $namaFotokepsek,
      'logo' => $namaLogo,
      'banner' => $namaBanner,
      'created_at' => time(),
      'updated_at' => time(),
    ]);

    session()->setFlashdata('pesan', 'Data berhasil diubah.');

    return redirect()->to('/administrator/setting/' . $slug);
  }

  //--------------------------------------------------------------------

}
