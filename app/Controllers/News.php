<?php

namespace App\Controllers;

use App\Models\NewsModel;

class News extends BaseController
{
  protected $newsModel;

  public function __construct()
  {
    $this->newsModel = new NewsModel();
  }

  public function index()
  {
    $data = [
      'title' => 'List Berita',
      'news' => $this->newsModel->getNews(),
      'setting' => $this->settingModel->getSetting(),
      'lihat' => $this->settingModel->getSetting(),
    ];

    return view('news/index', $data);
  }

  public function create()
  {
    // session(); ditaruh di basecontroller
    $data = [
      'title' => 'Form Tambah Data Berita',
      'setting' => $this->settingModel->getSetting(),
      'lihat' => $this->settingModel->getSetting(),
      'validation' => \Config\Services::validation()
    ];

    return view('news/create', $data);
  }

  public function save()
  {
    // Validasi input
    if (!$this->validate([
      'title' => [
        'rules' => 'required|is_unique[news.title]',
        'errors' => [
          'required' => '{field} news harus diisi.',
          'is_unique' => '{field} news sudah terdaftar'
        ]
      ],
      'news' => [
        'rules' => 'max_size[news,1024]|is_image[news]|mime_in[news,image/jpg,image/jpeg,image/png]',
        'errors' => [
          'max_size' => 'Ukuran gambar terlalu besar',
          'is_image' => 'Yang anda pilih bukan gambar',
          'mime_in' => 'Yang anda pilih bukan gambar'
        ]
      ]
    ])) {

      return redirect()->to('/administrator/news/create')->withInput();
    }

    // Ambil gambar
    $fileNews = $this->request->getFile('news');
    // Apakah tidak ada gambar yang diupload
    if ($fileNews->getError() == 4) {
      $namaNews = 'default.jpg';
    } else {
      // generate nama news random
      $namaNews = $fileNews->getRandomName();
      // Pindahlan file ke folder news
      $fileNews->move('news', $namaNews);
    }

    $slug = url_title($this->request->getVar('title'), '-', true);
    $this->newsModel->save([
      'title' => $this->request->getVar('title'),
      'slug' => $slug,
      'isi_berita' => $this->request->getVar('isi_berita'),
      'paragraf' => $this->request->getVar('paragraf'),
      'created_at' => time(),
      'updated_at' => time(),
      'news' => $namaNews
    ]);

    session()->setFlashdata('pesan', 'Data berhasil ditambahkan.');

    return redirect()->to('/administrator/news');
  }

  public function delete($id)
  {
    // Cari gambar bedasarkan id
    $news = $this->newsModel->find($id);

    // cek jika gambarnya default.jpg
    if ($news['news'] != 'default.jpg') {
      // Hapus gambar
      unlink('news/' . $news['news']);
    }

    $this->newsModel->delete($id);
    // session()->setFlashdata('pesan', 'Data berhasil dihapus.');

    return redirect()->to('/administrator/news');
  }

  public function edit($slug)
  {
    $data = [
      'title' => 'Form Ubah Data Berita',
      'validation' => \Config\Services::validation(),
      'news' => $this->newsModel->getNews($slug),
      'setting' => $this->settingModel->getSetting(),
      'lihat' => $this->settingModel->getSetting(),
    ];

    return view('news/edit', $data);
  }

  public function update($id)
  {
    //  Cek Judul
    $newsLama = $this->newsModel->getNews($this->request->getVar('slug'));
    if ($newsLama['title'] == $this->request->getVar('title')) {
      $rule_title = 'required';
    } else {
      $rule_title = 'required|is_unique[news.title]';
    }
    if (!$this->validate([
      'title' => [
        'rules' => $rule_title,
        'errors' => [
          'required' => '{field} news harus diisi.',
          'is_unique' => '{field} news sudah terdaftar'
        ]
      ],
      'news' => [
        'rules' => 'max_size[news,1024]|is_image[news]|mime_in[news,image/jpg,image/jpeg,image/png]',
        'errors' => [
          'max_size' => 'Ukuran gambar terlalu besar',
          'is_image' => 'Yang anda pilih bukan gambar',
          'mime_in' => 'Yang anda pilih bukan gambar'
        ]
      ]
    ])) {
      return redirect()->to('/administrator/news/edit/' . $this->request->getVar('slug'))->withInput();
    }

    $fileNews = $this->request->getFile('news');

    // Cek gambar, apakah tetap gambar lama
    if ($fileNews->getError() == 4) {
      $namaNews = $this->request->getVar('newsLama');
    } else {
      // generate nama file random
      $namaNews = $fileNews->getRandomName();
      // pindahkan gambar
      $fileNews->move('news', $namaNews);
      // Hapus file yang lama
      unlink('news/' . $this->request->getVar('newsLama'));
    }

    $slug = url_title($this->request->getVar('title'), '-', true);
    $this->newsModel->save([
      'id' => $id,
      'title' => $this->request->getVar('title'),
      'slug' => $slug,
      'paragraf' => $this->request->getVar('paragraf'),
      'isi_berita' => $this->request->getVar('isi_berita'),
      'updated_at' => time(),
      'news' => $namaNews
    ]);

    session()->setFlashdata('pesan', 'Data berhasil diubah.');

    return redirect()->to('/administrator/news');
  }

  //--------------------------------------------------------------------

}
