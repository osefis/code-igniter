<?php

namespace App\Controllers;

class Member extends BaseController
{

    public function index()
    {
        $data = [
            'title' => 'Halaman User',
            'lihat' => $this->settingModel->getSetting(),
            'setting' => $this->settingModel->getSetting()
        ];
        return view('member/index', $data);
    }
}
