<?php

namespace App\Controllers;

use App\Models\GaleriesModel;

class Album extends BaseController
{
  protected $galeriesModel;

  public function __construct()
  {
    $this->galeriesModel = new GaleriesModel();
  }

  public function index()
  {
    // $galeries = $this->galeriesModel->findAll();
    $data = [
      'title' => 'Galeries | Ayosinau',
      'galeries' => $this->galeriesModel->getGaleries(),
      'setting' => $this->settingModel->getSetting(),
    ];

    return view('album', $data);
  }

  //--------------------------------------------------------------------

}
