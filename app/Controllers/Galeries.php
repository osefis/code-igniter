<?php

namespace App\Controllers;

use App\Models\GaleriesModel;

class Galeries extends BaseController
{
  protected $galeriesModel;

  public function __construct()
  {
    $this->galeriesModel = new GaleriesModel();
  }

  public function index()
  {
    $data = [
      'title' => 'Galeries | Ayosinau',
      'galeries' => $this->galeriesModel->getGaleries(),
      'lihat' => $this->settingModel->getSetting(),
      'setting' => $this->settingModel->getSetting(),
    ];

    return view('galeries/index', $data);
  }

  public function create()
  {
    $data = [
      'title' => 'Form Tambah Data Galeries | Ayosinau',
      'setting' => $this->settingModel->getSetting(),
      'lihat' => $this->settingModel->getSetting(),
      'validation' => \Config\Services::validation()
    ];

    return view('galeries/create', $data);
  }

  public function save()
  {
    // Validasi input
    if (!$this->validate([
      'title' => [
        'rules' => 'required|is_unique[galeries.title]',
        'errors' => [
          'required' => '{field} galeries harus diisi.',
          'is_unique' => '{field} galeries sudah terdaftar'
        ]
      ],
      'galeri' => [
        'rules' => 'max_size[galeri,1024]|is_image[galeri]|mime_in[galeri,image/jpg,image/jpeg,image/png]',
        'errors' => [
          'max_size' => 'Ukuran gambar terlalu besar',
          'is_image' => 'Yang anda pilih bukan gambar',
          'mime_in' => 'Yang anda pilih bukan gambar'
        ]
      ]
    ])) {

      return redirect()->to('/administrator/galeries/create')->withInput();
    }

    // Ambil gambar
    $fileGaleri = $this->request->getFile('galeri');
    // Apakah tidak ada gambar yang diupload
    if ($fileGaleri->getError() == 4) {
      $namaGaleri = 'default.jpg';
    } else {
      // generate nama image random
      $namaGaleri = $fileGaleri->getRandomName();
      // Pindahlan file ke folder galeri
      $fileGaleri->move('galeri', $namaGaleri);
    }

    $slug = url_title($this->request->getVar('title'), '-', true);
    $this->galeriesModel->save([
      'title' => $this->request->getVar('title'),
      'slug' => $slug,
      'category' => $this->request->getVar('category'),
      'created_at' => time(),
      'updated_at' => time(),
      'galeri' => $namaGaleri
    ]);

    session()->setFlashdata('pesan', 'Data berhasil ditambahkan.');

    return redirect()->to('/administrator/galeries');
  }

  public function delete($id)
  {
    // Cari gambar bedasarkan id
    $galeries = $this->galeriesModel->find($id);

    // cek jika gambarnya default.jpg
    if ($galeries['galeri'] != 'default.jpg') {
      // Hapus gambar
      unlink('galeri/' . $galeries['galeri']);
    }

    $this->galeriesModel->delete($id);
    // session()->setFlashdata('pesan', 'Data berhasil dihapus.');

    return redirect()->to('/administrator/galeries');
  }

  public function edit($slug)
  {
    $data = [
      'title' => 'Form Ubah Data Galeries | Ayosinau',
      'validation' => \Config\Services::validation(),
      'setting' => $this->settingModel->getSetting(),
      'lihat' => $this->settingModel->getSetting(),
      'galeries' => $this->galeriesModel->getGaleries($slug)
    ];

    return view('galeries/edit', $data);
  }

  public function update($id)
  {
    //  Cek Judul
    $galeriesLama = $this->galeriesModel->getGaleries($this->request->getVar('slug'));
    if ($galeriesLama['title'] == $this->request->getVar('title')) {
      $rule_title = 'required';
    } else {
      $rule_title = 'required|is_unique[galeries.title]';
    }
    if (!$this->validate([
      'title' => [
        'rules' => $rule_title,
        'errors' => [
          'required' => '{field} galeries harus diisi.',
          'is_unique' => '{field} galeries sudah terdaftar'
        ]
      ],
      'galeri' => [
        'rules' => 'max_size[galeri,1024]|is_image[galeri]|mime_in[galeri,image/jpg,image/jpeg,image/png]',
        'errors' => [
          'max_size' => 'Ukuran gambar terlalu besar',
          'is_image' => 'Yang anda pilih bukan gambar',
          'mime_in' => 'Yang anda pilih bukan gambar'
        ]
      ]
    ])) {
      return redirect()->to('/administrator/galeries/edit/' . $this->request->getVar('slug'))->withInput();
    }

    $fileGaleri = $this->request->getFile('galeri');

    // Cek gambar, apakah tetap gambar lama
    if ($fileGaleri->getError() == 4) {
      $namaGaleri = $this->request->getVar('galeriLama');
    } else {
      // generate nama file random
      $namaGaleri = $fileGaleri->getRandomName();
      // pindahkan gambar
      $fileGaleri->move('galeri', $namaGaleri);
      // Hapus file yang lama
      unlink('galeri/' . $this->request->getVar('galeriLama'));
    }

    $slug = url_title($this->request->getVar('title'), '-', true);
    $this->galeriesModel->save([
      'id' => $id,
      'title' => $this->request->getVar('title'),
      'slug' => $slug,
      'category' => $this->request->getVar('category'),
      'updated_at' => time(),
      'galeri' => $namaGaleri
    ]);

    session()->setFlashdata('pesan', 'Data berhasil diubah.');

    return redirect()->to('/administrator/galeries');
  }

  //--------------------------------------------------------------------

}
