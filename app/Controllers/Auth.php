<?php

namespace App\Controllers;

use Config\Email;
use CodeIgniter\Controller;
use PhpParser\Node\Stmt\Return_;

// use Myth\Auth\Entities\User;

class Auth extends BaseController
{
    protected $auth;
    /**
     * @var Auth
     */
    protected $config;

    /**
     * @var \CodeIgniter\Session\Session
     */
    protected $session;

    public function __construct()
    {
        // Most services in this controller require
        // the session to be started - so fire it up!
        $this->session = service('session');

        $this->config = config('Auth');
        $this->auth = service('authentication');
    }

    public function login()
    {
        return view('auth/login');
    }
}
