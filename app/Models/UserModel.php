<?php

namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model
{
  protected $table      = 'users';
  protected $useTimestamps = true;
  protected $allowedFields = ['nama', 'slug', 'email', 'username', 'password_hash', 'is_active', 'role_id', 'user_image', 'active'];

  public function getUser($slug = false)
  {
    if ($slug == false) {
      return $this->findAll();
    }

    return $this->where(['slug' => $slug])->first();
  }
}
