  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?= base_url(); ?>/user/<?= user()->user_image; ?>" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="profil" class="d-block"><?= user()->nama; ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">

        <?php if (in_groups('admin')) : ?>

          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

            <!-- CODING MENU -->

            <?php
            // $role_id = $this->session->userdata('role_id');
            // $queryMenu = "SELECT    `user_menu`.`id`,`menu`
            //               FROM      `user_menu` JOIN `user_access_menu`.`menu_id`
            //               ON        `user_menu`.`id` = `user_access_menu`.`menu_id`
            //               WHERE     `user_access_menu`.`role_id` = $role_id
            //               ORDER BY  `user_access_menu`.`menu_id` ASC
            // ";

            // $menu = $this->db->query($queryMenu)->result_array();
            // var_dump($menu);
            // die;
            // 
            ?>

            <!-- END CODING MENU -->

            <li class="nav-item has-treeview menu-open">
              <a href="#" class="nav-link active">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>
                  Control Panel
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="<?= base_url(); ?>/administrator/dashboard" class="nav-link active">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Dashboard</p>
                  </a>
                </li>
                <li class="nav-item">
                  <?php foreach ($lihat as $l) : ?>
                    <a href="/administrator/setting/<?= $l['slug']; ?>" class="nav-link">
                    <?php endforeach; ?>
                    <i class="far fa-circle nav-icon"></i>
                    <p>Profil Sekolah</p>
                    </a>
                </li>
              </ul>
            </li>
            <li class="nav-item has-treeview">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-copy"></i>
                <p>
                  Modul
                  <i class="fas fa-angle-left right"></i>
                  <span class="badge badge-info right">6</span>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="<?= base_url(); ?>/administrator/sliders" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>slider</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="<?= base_url(); ?>/administrator/news" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Berita</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="<?= base_url(); ?>/administrator/galeries" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Album</p>
                  </a>
                </li>

              </ul>
            </li>
            <li class="nav-header">LABELS</li>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="nav-icon far fa-circle text-info"></i>
                <p>Log Update</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?= base_url() ?>/logout" class="nav-link">
                <i class="nav-icon far fa-circle text-danger"></i>
                <p>Logout</p>
              </a>
            </li>

          </ul>
        <?php endif; ?>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>