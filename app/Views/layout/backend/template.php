<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?= $title; ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url('plugins/fontawesome-free/css/all.min.css'); ?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="<?= base_url('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css'); ?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?= base_url('plugins/icheck-bootstrap/icheck-bootstrap.min.css'); ?>">
  <!-- JQVMap -->
  <link rel="stylesheet" href="<?= base_url('plugins/jqvmap/jqvmap.min.css'); ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url('dist/css/adminlte.min.css'); ?>">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?= base_url('plugins/overlayScrollbars/css/OverlayScrollbars.min.css'); ?>">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?= base_url('plugins/daterangepicker/daterangepicker.css'); ?>">
  <!-- summernote -->
  <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.15/dist/summernote-bs4.min.css" rel="stylesheet">
  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="<?= base_url(); ?>/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
  <!-- Toastr -->
  <link rel="stylesheet" href="<?= base_url(); ?>/plugins/toastr/toastr.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <!-- TABLE -->
  <!-- DataTables -->
  <link rel="stylesheet" href="<?= base_url('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('plugins/datatables-responsive/css/responsive.bootstrap4.min.css'); ?>">
  <!-- END TABEL -->

  <!-- My CSS -->
  <link rel="stylesheet" href="<?= base_url('/css/style.css'); ?>">
</head>

<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">

    <?= $this->include('layout/backend/navbar'); ?>
    <?= $this->include('layout/backend/sidebar'); ?>



    <?= $this->renderSection('content'); ?>

  </div>
  <!-- /.card-body -->

  <!-- /.card -->

  </section>
  <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?= $this->include('layout/backend/footer'); ?>

  <!-- jQuery -->
  <script src="<?= base_url('plugins/jquery/jquery.min.js'); ?>"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="<?= base_url('plugins/jquery-ui/jquery-ui.min.js'); ?>"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
    $.widget.bridge('uibutton', $.ui.button)
  </script>
  <!-- Bootstrap 4 -->
  <script src="<?= base_url('plugins/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
  <!-- ChartJS -->
  <script src="<?= base_url('plugins/chart.js/Chart.min.js'); ?>"></script>
  <!-- Sparkline -->
  <script src="<?= base_url('plugins/sparklines/sparkline.js'); ?>"></script>
  <!-- JQVMap -->
  <script src="<?= base_url('plugins/jqvmap/jquery.vmap.min.js'); ?>"></script>
  <script src="<?= base_url('plugins/jqvmap/maps/jquery.vmap.usa.js'); ?>"></script>
  <!-- jQuery Knob Chart -->
  <script src="<?= base_url('plugins/jquery-knob/jquery.knob.min.js'); ?>"></script>
  <!-- daterangepicker -->
  <script src="<?= base_url('plugins/moment/moment.min.js'); ?>"></script>
  <script src="<?= base_url('plugins/daterangepicker/daterangepicker.js'); ?>"></script>
  <!-- Tempusdominus Bootstrap 4 -->
  <script src="<?= base_url('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js'); ?>"></script>
  <!-- Summernote -->
  <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.15/dist/summernote-bs4.min.js"></script>
  <!-- overlayScrollbars -->
  <script src="<?= base_url('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js'); ?>"></script>
  <!-- AdminLTE App -->
  <script src="<?= base_url('dist/js/adminlte.js'); ?>"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <script src="<?= base_url('dist/js/pages/dashboard.js'); ?>"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="<?= base_url('dist/js/demo.js'); ?>"></script>

  <!-- TABEL -->
  <!-- DataTables -->
  <script src="<?= base_url('plugins/datatables/jquery.dataTables.min.js'); ?>"></script>
  <script src="<?= base_url('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js'); ?>"></script>
  <script src="<?= base_url('plugins/datatables-responsive/js/dataTables.responsive.min.js'); ?>"></script>
  <script src="<?= base_url('plugins/datatables-responsive/js/responsive.bootstrap4.min.js'); ?>"></script>

  <!-- SweetAlert2 -->
  <script src="<?= base_url(); ?>/plugins/sweetalert2/sweetalert2.min.js"></script>
  <!-- Toastr -->
  <script src="<?= base_url(); ?>/plugins/toastr/toastr.min.js"></script>

  <script>
    $(function() {
      $("#example1").DataTable({
        "responsive": true,
        "autoWidth": false,
      });
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
      });
    });
  </script>
  <!-- END TABLE -->

  <script>
    function previewImg() {
      const image = document.querySelector('#image');
      const imageLabel = document.querySelector('.custom-file-label');
      const imgPreview = document.querySelector('.img-preview');

      imageLabel.textContent = image.files[0].name;

      const fileImage = new FileReader();
      fileImage.readAsDataURL(image.files[0]);

      fileImage.onload = function(e) {
        imgPreview.src = e.target.result;
      }
    }
  </script>

  <script>
    function previewImgLogo() {
      const logo = document.querySelector('#logo');
      const logoLabel = document.querySelector('.custom-file-label-logo');
      const imgPreviewLogo = document.querySelector('.img-preview-logo');

      logoLabel.textContent = logo.files[0].name;

      const fileLogo = new FileReader();
      fileLogo.readAsDataURL(logo.files[0]);

      fileLogo.onload = function(e) {
        imgPreviewLogo.src = e.target.result;
      }
    }
  </script>

  <script>
    function previewImgUser_image() {
      const user_image = document.querySelector('#user_image');
      const user_imageLabel = document.querySelector('.custom-file-label');
      const imgPreviewUser_image = document.querySelector('.img-preview-user_image');

      user_imageLabel.textContent = user_image.files[0].name;

      const fileUser_image = new FileReader();
      fileUser_image.readAsDataURL(user_image.files[0]);

      fileUser_image.onload = function(e) {
        imgPreviewUser_image.src = e.target.result;
      }
    }
  </script>

  <script>
    function previewImgBanner() {
      const banner = document.querySelector('#banner');
      const bannerLabel = document.querySelector('.custom-file-label-banner');
      const imgPreviewBanner = document.querySelector('.img-preview-banner');

      bannerLabel.textContent = banner.files[0].name;

      const fileBanner = new FileReader();
      fileBanner.readAsDataURL(banner.files[0]);

      fileBanner.onload = function(e) {
        imgPreviewBanner.src = e.target.result;
      }
    }
  </script>

  <script>
    function previewImgFotokepsek() {
      const fotokepsek = document.querySelector('#fotokepsek');
      const fotokepsekLabel = document.querySelector('.custom-file-label-fotokepsek');
      const imgPreviewFotokepsek = document.querySelector('.img-preview-fotokepsek');

      fotokepsekLabel.textContent = fotokepsek.files[0].name;

      const fileFotokepsek = new FileReader();
      fileFotokepsek.readAsDataURL(fotokepsek.files[0]);

      fileFotokepsek.onload = function(e) {
        imgPreviewFotokepsek.src = e.target.result;
      }
    }
  </script>

  <script>
    function previewImgModul() {
      const bg_modul = document.querySelector('#bg_modul');
      const bg_modulLabel = document.querySelector('.custom-file-label');
      const imgPreviewModul = document.querySelector('.img-preview-modul');

      bg_modulLabel.textContent = bg_modul.files[0].name;

      const fileBg_modul = new FileReader();
      fileBg_modul.readAsDataURL(bg_modul.files[0]);

      fileBg_modul.onload = function(e) {
        imgPreviewModul.src = e.target.result;
      }
    }
  </script>

  <script>
    function previewImgSlider() {
      const slider = document.querySelector('#slider');
      const sliderLabel = document.querySelector('.custom-file-label');
      const imgPreviewSlider = document.querySelector('.img-preview-slider');

      sliderLabel.textContent = slider.files[0].name;

      const fileSlider = new FileReader();
      fileSlider.readAsDataURL(slider.files[0]);

      fileSlider.onload = function(e) {
        imgPreviewSlider.src = e.target.result;
      }
    }
  </script>
  <script>
    function previewImg() {
      const sarana = document.querySelector('#sarana');
      const saranaLabel = document.querySelector('.custom-file-label');
      const imgPreviewSarana = document.querySelector('.img-preview');

      saranaLabel.textContent = sarana.files[0].name;

      const fileSarana = new FileReader();
      fileSarana.readAsDataURL(sarana.files[0]);

      fileSarana.onload = function(e) {
        imgPreviewSarana.src = e.target.result;
      }
    }
  </script>

  <script>
    function previewImg() {
      const image = document.querySelector('#image');
      const imageLabel = document.querySelector('.custom-file-label');
      const imgPreviewImage = document.querySelector('.img-preview');

      imageLabel.textContent = image.files[0].name;

      const fileImage = new FileReader();
      fileImage.readAsDataURL(image.files[0]);

      fileImage.onload = function(e) {
        imgPreviewImage.src = e.target.result;
      }
    }
  </script>

  <script>
    function previewImgGaleri() {
      const galeri = document.querySelector('#galeri');
      const galeriLabel = document.querySelector('.custom-file-label');
      const imgPreviewGaleri = document.querySelector('.img-preview-galeri');

      galeriLabel.textContent = galeri.files[0].name;

      const fileGaleri = new FileReader();
      fileGaleri.readAsDataURL(galeri.files[0]);

      fileGaleri.onload = function(e) {
        imgPreviewGaleri.src = e.target.result;
      }
    }
  </script>

  <script>
    function previewImgNews() {
      const news = document.querySelector('#news');
      const newsLabel = document.querySelector('.custom-file-label');
      const imgPreviewNews = document.querySelector('.img-preview-news');

      newsLabel.textContent = news.files[0].name;

      const fileNews = new FileReader();
      fileNews.readAsDataURL(news.files[0]);

      fileNews.onload = function(e) {
        imgPreviewNews.src = e.target.result;
      }
    }
  </script>

  <?= $this->renderSection('extra-js') ?>

  <script>
    $(document).ready(function() {
      $('#sejarah').summernote({
        tabsize: 2,
        height: 500
      });
    })
  </script>

  <script>
    $(document).ready(function() {
      $('#visimisi').summernote({
        tabsize: 2,
        height: 500
      });
    })
  </script>

  <script>
    $(document).ready(function() {
      $('#isi_berita').summernote({
        tabsize: 2,
        height: 500
      });
    })
  </script>


</body>

</html>