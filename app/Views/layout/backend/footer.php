  <footer class="main-footer">
    <a href="https://www.divisidatalitbang.net" target="_blank"><strong>DIVISIDATALITBANG</a> &copy; 2015-<?= date('Y'); ?> ---</strong> Powered by <a href="http://adminlte.io" target="_blank">AdminLTE.io</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 1.0.0
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->