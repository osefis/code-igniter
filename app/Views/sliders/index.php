<?= $this->extend('layout/backend/template'); ?>

<?= $this->section('content'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><?= $title; ?></h1>
        </div>
        <div class="col-sm-6">

        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">
          <a href="<?= base_url('/administrator/sliders/create'); ?>" class="btn btn-sm btn-primary">Tambah Data Slider</a>
        </h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fas fa-minus"></i></button>
          <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fas fa-times"></i></button>
        </div>
      </div>
      <div class="card-body">

        <!-- Default box -->

        <!-- END HEADER -->

        <div class="container">
          <div class="row">
            <div class="col">

              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Gambar</th>
                    <th scope="col">Judul</th>
                    <th scope="col">Posisi gambar</th>
                    <th scope="col">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $i = 1; ?>
                  <?php foreach ($sliders as $s) : ?>
                    <tr>
                      <th scope="row"><?= $i++; ?></th>
                      <td><img src="/slider/<?= $s['slider']; ?>" alt="" class="image" height="75"></td>
                      <td><?= $s['title']; ?></td>
                      <td><?= $s['status']; ?></td>
                      <td>
                        <a href="/administrator/sliders/edit/<?= $s['slug']; ?>" class="btn btn-xs btn-warning">Edit</a>
                        <form action="/administrator/sliders/delete/<?= $s['id']; ?>" method="post" class="d-inline">
                          <input type="hidden" name="_method" value="DELETE">
                          <?= csrf_field(); ?>
                          <button type="submit" class="btn btn-xs btn-danger" onclick="return confirm('Apakah anda Yakin?');">Delete</button>
                        </form>
                      </td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>

            </div>
          </div>
        </div>

        <?= $this->endSection(); ?>