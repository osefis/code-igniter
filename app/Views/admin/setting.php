<?= $this->extend('layout/backend/template'); ?>

<?= $this->section('content'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><?= $title; ?> <?= $setting['sekolah']; ?></h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">

          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="card card-primary card-outline">
            <div class="card-body box-profile">
              <div class="text-center">
                <img class="profile-user-img img-fluid img-circle" src="/setting/<?= $setting['fotokepsek']; ?>" alt="User profile picture">
              </div>

              <h3 class="profile-username text-center"><?= $setting['kepsek']; ?></h3>

              <p class="text-muted text-center">Kepala Sekolah</p>

              <ul class="list-group list-group-unbordered mb-3">
                <li class="list-group-item">
                  <b>NPSN</b> <a class="float-right"><?= $setting['npsn']; ?></a>
                </li>
                <li class="list-group-item">
                  <b>Telp/Fax</b> <a class="float-right"><?= $setting['telp']; ?></a>
                </li>
              </ul>

              <a href="https://www.facebook.com/<?= $setting['facebook']; ?>" class="btn btn-primary btn-block" target="_blank"><b>Facebook</b></a>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

          <!-- About Me Box -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Media Sosial</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <strong><i class="fas fa-book mr-1"></i> Alamat</strong>

              <p class="text-muted">
                <?= $setting['alamat']; ?>
              </p>

              <hr>
              <a href="https://www.twitter.com/<?= $setting['twitter']; ?>" target="_blank"><strong><i class="fab fa-twitter mr-1"></i> Twitter | </strong></a>
              <a href="https://www.instagram.com/<?= $setting['instagram']; ?>" target="_blank"><strong><i class="fab fa-instagram mr-1"></i>Instagram</strong></a>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="card">
            <div class="card-header p-2">
              <ul class="nav nav-pills">
                <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Visi dan Misi</a></li>
                <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">Sejarah</a></li>
                <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Profil</a></li>
              </ul>
            </div><!-- /.card-header -->
            <div class="card-body">
              <div class="tab-content">

                <div class="active tab-pane" id="activity">
                  <!-- Post -->

                  <form action="/setting/update/<?= $setting['id']; ?>" method="post" enctype="multipart/form-data">
                    <?= csrf_field(); ?>
                    <input type="hidden" name="slug" value="<?= $setting['slug']; ?>">
                    <input type="hidden" name="logoLama" value="<?= $setting['logo']; ?>">
                    <input type="hidden" name="bannerLama" value="<?= $setting['banner']; ?>">
                    <input type="hidden" name="fotokepsekLama" value="<?= $setting['fotokepsek']; ?>">
                    <!-- The timeline -->

                    <div class="form-group row">
                      <label for="logo" class="col-sm-2 col-form-label">Logo Sekolah</label>
                      <div class="col-sm-2">
                        <img src="/setting/<?= $setting['logo']; ?>" class="img-thumbnail img-preview-logo">
                      </div>
                      <div class="col-sm-8">
                        <div class="custom-file">
                          <input type="file" class="custom-file-input <?= ($validation->hasError('logo')) ? 'is-invalid' : ''; ?>" id="logo" name="logo" onchange="previewImgLogo()">
                          <div class="invalid-feedback">
                            <?= $validation->getError('logo'); ?>
                          </div>
                          <label class="custom-file-label-logo" for="logo"><?= $setting['logo']; ?></label>
                        </div>
                      </div>
                    </div>

                    <div class="form-group row">
                      <div class="col-sm-12">
                        <textarea name="visimisi" id="visimisi" class="form-control" required><?= $setting['visimisi']; ?></textarea>
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-12">
                        <button type="submit" class="btn btn-danger">Submit</button>
                      </div>
                    </div>

                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="timeline">

                  <!-- The timeline -->
                  <div class="form-group row">
                    <label for="banner" class="col-sm-2 col-form-label">Banner Sekolah</label>
                    <div class="col-sm-2">
                      <img src="/setting/<?= $setting['banner']; ?>" class="img-thumbnail img-preview-banner">
                    </div>
                    <div class="col-sm-8">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input <?= ($validation->hasError('banner')) ? 'is-invalid' : ''; ?>" id="banner" name="banner" onchange="previewImgBanner()">
                        <div class="invalid-feedback">
                          <?= $validation->getError('banner'); ?>
                        </div>
                        <label class="custom-file-label-banner" for="banner"><?= $setting['banner']; ?></label>
                      </div>
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="col-sm-12">
                      <textarea name="sejarah" id="sejarah" class="form-control" required><?= $setting['sejarah']; ?></textarea>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-12">
                      <button type="submit" class="btn btn-danger">Submit</button>
                    </div>
                  </div>

                </div>
                <!-- /.tab-pane -->

                <div class="tab-pane" id="settings">


                  <div class="form-group row">
                    <label for="npsn" class="col-sm-2 col-form-label">NPSN</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="npsn" name="npsn" value="<?= (old('npsn')) ? old('npsn') : $setting['npsn']; ?>">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="sekolah" class="col-sm-2 col-form-label">Nama Sekolah</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control <?= ($validation->hasError('sekolah')) ? 'is-invalid' : ''; ?>" id="sekolah" name="sekolah" autofocus value="<?= (old('sekolah')) ? old('sekolah') : $setting['sekolah']; ?>">
                      <div class="invalid-feedback">
                        <?= $validation->getError('sekolah'); ?>
                      </div>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="domain" class="col-sm-2 col-form-label">Nama Domain</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="domain" name="domain" value="<?= (old('domain')) ? old('domain') : $setting['domain']; ?>">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="telp" class="col-sm-2 col-form-label">Telp/Fax</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="telp" name="telp" value="<?= (old('telp')) ? old('telp') : $setting['telp']; ?>">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="kepsek" class="col-sm-2 col-form-label">Nama Kepala Sekolah</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="kepsek" name="kepsek" value="<?= (old('kepsek')) ? old('kepsek') : $setting['kepsek']; ?>">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="pengantar" class="col-sm-2 col-form-label">Pengantar</label>
                    <div class="col-sm-10">
                      <textarea name="pengantar" id="pengantar" class="form-control" required><?= $setting['pengantar']; ?></textarea>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
                    <div class="col-sm-10">
                      <textarea name="alamat" id="alamat" class="form-control" required><?= $setting['alamat']; ?></textarea>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="fotokepsek" class="col-sm-2 col-form-label">Foto Kepala Sekolah</label>
                    <div class="col-sm-2">
                      <img src="/setting/<?= $setting['fotokepsek']; ?>" class="img-thumbnail img-preview-fotokepsek">
                    </div>
                    <div class="col-sm-8">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input <?= ($validation->hasError('fotokepsek')) ? 'is-invalid' : ''; ?>" id="fotokepsek" name="fotokepsek" onchange="previewImgFotokepsek()">
                        <div class="invalid-feedback">
                          <?= $validation->getError('fotokepsek'); ?>
                        </div>
                        <label class="custom-file-label-fotokepsek" for="fotokepsek"><?= $setting['fotokepsek']; ?></label>
                      </div>
                    </div>
                  </div>



                  <div class="form-group row">
                    <label for="lokasi" class="col-sm-2 col-form-label">Lokasi</label>
                    <div class="col-sm-10">
                      <textarea name="lokasi" id="lokasi" class="form-control" required><?= $setting['lokasi']; ?></textarea>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="video" class="col-sm-2 col-form-label">Video Profil Sekolah</label>
                    <div class="col-sm-10">
                      <textarea name="video" id="video" class="form-control" required><?= $setting['video']; ?></textarea>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="facebook" class="col-sm-2 col-form-label">Facebook</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="facebook" name="facebook" value="<?= (old('facebook')) ? old('facebook') : $setting['facebook']; ?>">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="twitter" class="col-sm-2 col-form-label">Twitter</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="twitter" name="twitter" value="<?= (old('twitter')) ? old('twitter') : $setting['twitter']; ?>">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="instagram" class="col-sm-2 col-form-label">Instagram</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="instagram" name="instagram" value="<?= (old('instagram')) ? old('instagram') : $setting['instagram']; ?>">
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="offset-sm-2 col-sm-10">
                      <button type="submit" class="btn btn-danger">Submit</button>
                    </div>
                  </div>

                  </form>

                </div>



                <!-- /.tab-pane -->
              </div>
              <!-- /.tab-content -->
            </div><!-- /.card-body -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?= $this->endSection('') ?>