<?= $this->extend('layout/backend/template'); ?>

<?= $this->section('content'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><?= $title; ?></h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><?= $title; ?></li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">
          <a href="<?= base_url('/administrator/news'); ?>" class="btn btn-sm btn-default">Kembali</a>
        </h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fas fa-minus"></i></button>
          <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fas fa-times"></i></button>
        </div>
      </div>
      <div class="card-body">

        <!-- Default box -->

        <!-- END HEADER -->

        <div class="container">
          <div class="row">
            <div class="col-8">

              <h2 class="my-3">Form Ubah Data Berita</h2>

              <form action="/news/update/<?= $news['id']; ?>" method="post" enctype="multipart/form-data">
                <?= csrf_field(); ?>
                <input type="hidden" name="slug" value="<?= $news['slug']; ?>">
                <input type="hidden" name="newsLama" value="<?= $news['news']; ?>">
                <div class="form-group row">
                  <label for="title" class="col-sm-2 col-form-label">Judul</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control <?= ($validation->hasError('title')) ? 'is-invalid' : ''; ?>" id="title" name="title" autofocus value="<?= (old('title')) ? old('title') : $news['title']; ?>">
                    <div class="invalid-feedback">
                      <?= $validation->getError('title'); ?>
                    </div>
                  </div>
                </div>

                <div class="form-group row">
                  <label for="paragraf" class="col-sm-2 col-form-label">Paragraf Pendahuluan</label>
                  <div class="col-sm-10">
                    <textarea name="paragraf" id="paragraf" class="form-control" required><?= $news['paragraf']; ?></textarea>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="isi_berita" class="col-sm-2 col-form-label">Isi Berita</label>
                  <div class="col-sm-10">
                    <textarea name="isi_berita" id="isi_berita" class="form-control" required><?= $news['isi_berita']; ?></textarea>
                  </div>
                </div>

                <div class="form-group row">
                  <label for="news" class="col-sm-2 col-form-label">Sampul</label>
                  <div class="col-sm-2">
                    <img src="/news/<?= $news['news']; ?>" class="img-thumbnail img-preview-news">
                  </div>
                  <div class="col-sm-8">
                    <div class="custom-file">
                      <input type="file" class="custom-file-input <?= ($validation->hasError('news')) ? 'is-invalid' : ''; ?>" id="news" name="news" onchange="previewImgNews()">
                      <div class="invalid-feedback">
                        <?= $validation->getError('news'); ?>
                      </div>
                      <label class="custom-file-label" for="news"><?= $news['news']; ?></label>
                    </div>
                  </div>
                </div>


                <div class="form-group row">
                  <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Ubah Data</button>
                  </div>
                </div>
              </form>

            </div>
          </div>
        </div>



        <?= $this->endSection(); ?>