<header id="header">
  <?php foreach ($setting as $s) : ?>
    <div class="top-bar">
      <div class="container">
        <div class="row">
          <div class="col-sm-6 col-xs-12">
            <div class="top-number">
              <p><i class="fa fa-phone-square"></i> <?= $s['telp']; ?></p>
            </div>
          </div>
          <div class="col-sm-6 col-xs-12">
            <div class="social">
              <ul class="social-share">
                <li><a href="https://www.facebook.com/<?= $s['facebook']; ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
                <li><a href="https://twitter.com/<?= $s['twitter']; ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
                <li><a href="https://www.instagram.com/<?= $s['instagram']; ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
              </ul>
              <div class="search">
                <form role="form">
                  <input type="text" class="search-form" autocomplete="off" placeholder="Search">
                  <i class="fa fa-search"></i>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--/.container-->
    </div>
    <!--/.top-bar-->

    <nav class="navbar navbar-inverse" role="banner">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?= base_url(); ?>/"><img src="<?= base_url(); ?>/setting/<?= $s['banner']; ?>" alt="logo" height="60"></a>
        </div>

        <div class="collapse navbar-collapse navbar-right">
          <ul class="nav navbar-nav">
            <li class="active"><a href="<?= base_url(); ?>/">Home</a></li>
            <li><a href="<?= base_url(); ?>/about-us">About Us</a></li>
            <li><a href="<?= base_url(); ?>/services">Services</a></li>
            <li><a href="<?= base_url(); ?>/album">Galeri</a></li>
            <li class="dropdown">
              <a href="<?= base_url(); ?>/#" class="dropdown-toggle" data-toggle="dropdown">Pages <i class="fa fa-angle-down"></i></a>
              <ul class="dropdown-menu">
                <li><a href="<?= base_url(); ?>/blog-item">Blog Single</a></li>
                <li><a href="<?= base_url(); ?>/pricing">Pricing</a></li>
                <li><a href="<?= base_url(); ?>/404">404</a></li>
              </ul>
            </li>
            <li><a href="<?= base_url(); ?>/berita">Berita</a></li>
            <li><a href="<?= base_url(); ?>/contact-us">Contact</a></li>
          </ul>
        </div>
      </div>
      <!--/.container-->
    </nav>
    <!--/nav-->
  <?php endforeach; ?>

</header>
<!--/header-->