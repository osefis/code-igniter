<?= $this->extend('layout/backend/template'); ?>

<?= $this->section('content'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><?= $title; ?></h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><?= $title; ?></li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">
          <a href="<?= base_url('/administrator/user'); ?>" class="btn btn-sm btn-default">Kembali</a>
        </h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fas fa-minus"></i></button>
          <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fas fa-times"></i></button>
        </div>
      </div>
      <div class="card-body">

        <!-- Default box -->

        <!-- END HEADER -->

        <div class="container">
          <div class="row">
            <div class="col-8">

              <form action="/user/save" method="post" enctype="multipart/form-data">
                <?= csrf_field(); ?>

                <div class="form-group row">
                  <label for="nama" class="col-sm-2 col-form-label">Nama</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control <?= ($validation->hasError('nama')) ? 'is-invalid' : ''; ?>" id="nama" name="nama" autofocus value="<?= old('nama'); ?>">
                    <div class="invalid-feedback">
                      <?= $validation->getError('nama'); ?>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="email" class="col-sm-2 col-form-label">Email</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control <?= ($validation->hasError('email')) ? 'is-invalid' : ''; ?>" id="email" name="email" autofocus value="<?= old('email'); ?>">
                    <div class="invalid-feedback">
                      <?= $validation->getError('email'); ?>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="password_hash" class="col-sm-2 col-form-label">Passowrd</label>
                  <div class="col-sm-10">
                    <input type="password" class="form-control <?= ($validation->hasError('password_hash')) ? 'is-invalid' : ''; ?>" id="password_hash" name="password_hash" autofocus value="<?= old('password_hash'); ?>">
                    <div class="invalid-feedback">
                      <?= $validation->getError('password_hash'); ?>
                    </div>
                  </div>
                </div>

                <div class="form-group row">
                  <label for="user_image" class="col-sm-2 col-form-label">Gambar</label>
                  <div class="col-sm-2">
                    <img src="<?= base_url('/user/avatar.png'); ?>" class="img-thumbnail img-preview-user_image">
                  </div>
                  <div class="col-sm-8">
                    <div class="custom-file">
                      <input type="file" class="custom-file-input <?= ($validation->hasError('user_image')) ? 'is-invalid' : ''; ?>" id="user_image" name="user_image" onchange="previewImgUser_image()">
                      <div class="invalid-feedback">
                        <?= $validation->getError('user_image'); ?>
                      </div>
                      <label class="custom-file-label" for="user_image">Pilih gambar..</label>
                    </div>
                  </div>
                </div>

                <div class="form-group row">
                  <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Tambah Data</button>
                  </div>
                </div>
              </form>

            </div>
          </div>
        </div>

        <?= $this->endSection(); ?>