<section id="content">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-10 col-sm-offset-1 fadeInDown">
        <div class="tab-wrap">
          <div class="media">
            <div class="parrent pull-left">
              <ul class="nav nav-tabs nav-stacked">
                <li class="active"><a href="<?= base_url(); ?>/#tab1" data-toggle="tab" class="analistic-01">Pengantar Kepala Sekolah</a></li>
                <li class=""><a href="<?= base_url(); ?>/#tab2" data-toggle="tab" class="analistic-02">Sejarah</a></li>
                <li class=""><a href="<?= base_url(); ?>/#tab3" data-toggle="tab" class="tehnical">Visi dan Misi</a></li>
                <li class=""><a href="<?= base_url(); ?>/#tab4" data-toggle="tab" class="tehnical">Lokasi</a></li>
                <li class=""><a href="<?= base_url(); ?>/#tab5" data-toggle="tab" class="tehnical">Video Profil</a></li>
              </ul>
            </div>

            <div class="parrent media-body">
              <div class="tab-content">
                <?php foreach ($setting as $s) : ?>
                  <div class="tab-pane fade active in text-right" id="tab1">
                    <div class="media">
                      <div class="pull-left">
                        <img class="img-responsive" src="<?= base_url(); ?>/setting/<?= $s['fotokepsek']; ?>" width="200">

                      </div>
                      <div class="media-body">
                        <h2><?= $s['kepsek']; ?></h2>
                        <p><?= $s['pengantar']; ?></p>
                      </div>
                    </div>
                  </div>

                  <div class="tab-pane fade" id="tab2">
                    <p><?= $s['sejarah']; ?></p>
                  </div>

                  <div class="tab-pane fade" id="tab3">
                    <p><?= $s['visimisi']; ?></p>
                  </div>

                  <div class="tab-pane fade" id="tab4">
                    <p><?= $s['lokasi']; ?></p>
                  </div>

                  <div class="tab-pane fade" id="tab5">
                    <div class="video-box">
                      <img src="<?= base_url(); ?>/images/tab-video-bg.png" alt="video">
                      <a class="video-icon" href="<?= base_url(); ?>/http://www.youtube.com/watch?v=<?= $s['video']; ?>" rel="prettyPhoto"><i class="fa fa-play"></i></a>
                    </div>
                  </div>
                <?php endforeach; ?>
              </div>
              <!--/.tab-content-->
            </div>
            <!--/.media-body-->
          </div>
          <!--/.media-->
        </div>
        <!--/.tab-wrap-->
      </div>
      <!--/.col-sm-6-->

    </div>
    <!--/.row-->
  </div>
  <!--/.container-->
</section>
<!--/#content-->